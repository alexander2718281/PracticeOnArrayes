package ru.mail.alexander_kurlovich3;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //arrayGeneration(10, 4);
        //arrayGenerationAndSearch(10);
        //arrayGenerationAndPrintSecondMaxValue(10);
        arraysGenerationAndSomeLogic(10,10);
    }

    public static int[] arrayGeneration(int arrayLenght, int growElement){
        int[] array = new int[arrayLenght];
        for (int i = 0; i < array.length; i++){
            array[i] = (int) (100000* Math.random());
            System.out.println(array[i]);
        }
        System.out.println("Test");
        array[growElement] = (int) (array[growElement]*1.1);
        for (int i : array) {
            System.out.println(i);
        }
        return array;
    }

    public static void arrayGenerationAndSearch(int arrayLenght){
        int[] array = new int[arrayLenght];
        for (int i = 0; i < array.length; i++){
            array[i] = (int) (100* Math.random());
            System.out.println(array[i]);
        }
        Scanner scanner = new Scanner(System.in);
        int search = scanner.nextInt();
        int numOfSeatchElement = -1;
        for (int i = 0; i < arrayLenght; i++){
            if (array[i] == search){
                numOfSeatchElement = i;
                break;
            }
        }
        if (numOfSeatchElement >= 0){
            int summ = 0;
            for (int i = 0; i < numOfSeatchElement; i++) {
                summ += array[i];
            }
            System.out.println("Summ before this element:" + summ);
            summ = 0;
            for (int i = numOfSeatchElement+1; i < arrayLenght; i++ ){
                summ += array[i];
            }
            System.out.println("Summ after this element:" + summ);
        }
        else{
            System.out.println("Нет");
        }
    }

    public static void arrayGenerationAndPrintSecondMaxValue(int arrayLenght){
        int[] array = new int[arrayLenght];
        for (int i = 0; i < array.length; i++){
            array[i] = (int) (100* Math.random());
            System.out.println(array[i]);
        }
        Arrays.sort(array);
        System.out.println("Second max value: " + array[arrayLenght-2]);
    }

    public static void arraysGenerationAndSomeLogic(int lenghtOfFirstArray, int lenghtOfSecondArray){
        System.out.println("First array:");
        int[] array1 = new int[lenghtOfFirstArray];
        for (int i = 0; i < array1.length; i++){
            array1[i] = (int) (100* Math.random());
            System.out.println(array1[i]);
        }
        System.out.println("Second array:");
        int[] array2 = new int[lenghtOfSecondArray];
        for (int i = 0; i < array2.length; i++) {
            array2[i] = (int) (100 * Math.random());
            System.out.println(array2[i]);
        }
        int numOfMinElement = 0;
        int minElement = array1[0];
        for (int i = 0; i < array1.length; i++) {
            if (array1[i] < minElement){
                minElement = array1[i];
                numOfMinElement = i;
            }
        }
        System.out.println("Corresponding element pf second array: " + array2[numOfMinElement]);
    }
}
